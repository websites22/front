import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import players from "./containers/players";
import teams from "./containers/teams";
import match from "./containers/match";
import { history } from "./history";
import PrivateRoute from "./containers/PrivateRoute";
import VerticalLayout from "./layouts/VerticalLayout";
import Login from "./containers/Login";
import Question from "./containers/Questions";
import Partner from "./containers/partner";

const Routers = () => {
  return (
    <Router history={history}>
      <Switch>
        <Route path="/login" component={Login} />

        <VerticalLayout>
          <PrivateRoute>
            <Route exact path="/players" component={players} />
            <Route exact path="/questions" component={Question} />
            <Route exact path="/partner" component={Partner} />
            <Route exact path="/teams" component={teams} />
            <Route exact path="/matchs" component={match} />
          </PrivateRoute>
        </VerticalLayout>
      </Switch>
    </Router>
  );
};
export default Routers;
