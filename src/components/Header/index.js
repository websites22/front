import React from "react";

export const Header = (
  { bg, title, height, bgColor, titreClassNames, classNames, containerStyle },
  props
) => {
  return (
    <div
      className={`page-header ${classNames}`}
      style={Object.assign(
        {
          backgroundColor: bgColor ? bgColor : "transparent",
          backgroundImage: `url(${bg})`,
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          height: height ? height : "120px",
        },
        containerStyle
      )}
    >
      {title && (
        <div
          className={`page-header-title font-semibold text-base ${titreClassNames}`}
        >
          {title}
        </div>
      )}
    </div>
  );
};
