import React from "react";
import * as Icons from "react-feather";
//import { Row } from "reactstrap";
import classNames from "classnames";
import { useHistory, withRouter } from "react-router-dom";
const logo = require("../../assets/img/logo.png");
const Item = ({ title, icon, active, onClick }) => {
  return (
    <div
      onClick={onClick}
      className={classNames(
        "d-flex p-3 pl-4",
        "siderbar-item",
        "cursor-pointer",
        active && "sidebar-active-item"
      )}
    >
      {icon}
      <span
        className={classNames(
          "ml-2 text-sm font-medium",
          active ? "text-black" : "text-gray-500"
        )}
      >
        {title}
      </span>
    </div>
  );
};
const SiderBar = (props) => {
  const history = useHistory();
  const [activeMenu, setActiveMenu] = React.useState("/home");
  function navigate(route) {
    setActiveMenu(route);
    history.push(route);
  }
  return (
    <div className="side-bar">
      <div className="logo-container d-flex justify-content-center align-items-center mt-5">
        <span className="text-lg logo text-gray-700">
          <img src={logo} alt="logo" />
        </span>
      </div>
      <div className="mt-5">
        <Item
          active={activeMenu === "/home"}
          title="JExperts"
          icon={
            <Icons.Home
              size={20}
              color="#604B9B"
              onClick={() => navigate("/home")}
            />
          }
        />
        <Item
          active={activeMenu === "/teams"}
          onClick={() => navigate("/teams")}
          title="Votre Secteur"
          icon={<Icons.List size={20} color="#604B9B" />}
        />
        <Item
          active={activeMenu === "/players"}
          onClick={() => navigate("/players")}
          title="Nos Offres"
          icon={<Icons.Gift size={20} color="#604B9B" />}
        />
        <Item
          active={activeMenu === "/matchs"}
          onClick={() => navigate("/matchs")}
          title="Nos Offres Sur Mesures  "
          icon={<Icons.ShoppingBag size={20} color="#604B9B" />}
        />
        <Item
          onClick={() => navigate("/partner")}
          active={activeMenu === "/partner"}
          title="Partenaires"
          icon={<Icons.Users size={20} color="#604B9B" />}
        />
        <Item
          active={activeMenu === "/questions"}
          onClick={() => navigate("/questions")}
          title="Questions fréquemment posées  "
          icon={<Icons.HelpCircle size={20} color="#604B9B" />}
        />
      </div>
    </div>
  );
};
export default withRouter(SiderBar);
