import React from "react";

export default function Footer() {
  return (
    <div>
      <footer className="footer">
        &copy; 2021 JCexperts, tous droits réservés
      </footer>
    </div>
  );
}
