import React from "react";
import {
  //Row,
  Col,
  //Input,
  UncontrolledDropdown,
  //	DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
//import { Search } from "react-feather";
import { useHistory } from "react-router-dom";
import { Header } from "../Header";

export default function NavBar() {
  const history = useHistory();
  function logout() {
    localStorage.removeItem("token");
    history.push("/home");
    window.location.reload();
    //redirect
  }
  return (
    <div className="nav-bar d-flex align-items-center">
      {/* <Col md="10" lg="10" className="d-flex align-items-center">
        <input
          type="text"
          placeholder="Recherche..."
          className="navbar-search-input"
        />
      </Col> */}
      <Col
        md="2"
        lg="2"
        sm="6"
        className="d-flex justify-content-start align-items-center"
      >
        <UncontrolledDropdown className="w-100">
          {/* <DropdownToggle caret color="white" className="d-flex align-items-center">
						<div className="d-flex justify-content-center align-items-center">
							<img
								alt="user"
								className="user-img"
								src="https://png.pngtree.com/png-vector/20190710/ourmid/pngtree-user-vector-avatar-png-image_1541962.jpg"
							/>
                            <div className="d-flex flex-column justify-content-start align-items-center">
                            <span className="text-xs ml-1">Foued jridi</span>
                           
                            </div>
							
						</div>
					</DropdownToggle> */}
          <DropdownMenu>
            <DropdownItem className="text-sm">Mon compte</DropdownItem>

            <DropdownItem className="text-sm"> Changer password</DropdownItem>

            <DropdownItem divider />
            <DropdownItem className="text-sm" onClick={logout}>
              {" "}
              Déconnecter
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
      </Col>
    </div>
  );
}
