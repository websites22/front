import React from "react";
import SiderBar from "../components/SideBar";
//import Footer from '../components/Footer'
import NavBar from "../components/NavBar";
import Footer from "../components/Footer";

export default function VerticalLayout(props) {
  return (
    <div className="app">
      <SiderBar />
      <main className="main-container">
        {/* <NavBar /> */}
        <div className="content">{props.children}</div>
        <Footer />
      </main>
    </div>
  );
}
