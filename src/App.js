import React from "react";
import Routers from "./Router";
import { ApolloProvider } from "react-apollo";
import client from "./config/apolloclient";

function App() {
  return (
    <ApolloProvider client={client}>
      <Routers />
    </ApolloProvider>
  );
}

export default App;
