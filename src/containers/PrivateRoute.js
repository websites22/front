import React from "react";
import Token from "../helpers/Token";
import { useHistory } from "react-router-dom";

export default function PrivateRoute(props) {
  const history = useHistory();
  React.useEffect(() => {
    const token = Token.getToken();

    if (!!!token) {
      history.push("/home");
    }
  }, []);

  return <div>{props.children}</div>;
}
