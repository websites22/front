import React from "react";
import { Card, Col, Input } from "reactstrap";
//import { Trash, Eye, Edit } from "react-feather";
import withConfirmation from "../../hoc/withconfirmation";
import {
  //useMutationPlayer,
  //useQueryPlayers,
  useMatchQuery,
  useMatchMutation,
} from "../../hooks/teams.hooks";
import { BASE_URL } from "../../config/constants";
import ModalDetails from "./ModalDetails";
import moment from "moment";
import classNames from "classnames";
import { Header } from "../../components/Header";
import bg from "../../assets/img/bg.jpg";

const STATUS_FR = {
  WAITING: "En attente",
  ACCEPTED: "Accepté",
  FINISHED: "Finis",
  REFUSED: "Refusé",
};
const STATUS_COLORS = {
  WAITING: "#bfba4d",
  ACCEPTED: "#2edb45",
  FINISHED: "red",
  REFUSED: "#d60f34",
};
const PlayerItem = ({
  id,
  handleDelete,
  localTeam,
  adversaryTeam,
  stadium,
  timeStartMatch,
  timeEndMatch,
  goalAdversaryTeam,
  goalLocalTeam,
  dateMatch,
  address,
  status,
  openDetails,
  match,
}) => {
  function renderScore() {
    if (status !== "FINISHED") {
      return <span>vs</span>;
    }
    return (
      <span className="font-bold text-xl">
        {goalLocalTeam}:{goalAdversaryTeam}
      </span>
    );
  }
  return (
    <div className="match-item">
      <Col md="12" sm="12" lg="12" className="d-flex justify-content-center ">
        <span
          style={{
            color: STATUS_COLORS[status],
          }}
          className="font-bold"
        >
          {STATUS_FR[status]}
        </span>
      </Col>
      <div className="d-flex">
        <Col className="d-flex" md="4" sm="4" lg="4">
          <div className="d-flex align-items-center">
            <img
              className="player-img"
              src={`${BASE_URL}${localTeam?.pictureUrl}`}
              alt="team"
            />
            <span className="text-xs ml-2">{localTeam?.name}</span>
          </div>
        </Col>
        <Col md="4" sm="4" lg="4" className="d-flex justify-content-center ">
          <div className="d-flex flex-column justify-content-center align-items-center">
            {renderScore()}
            <span>{moment(timeStartMatch).format("HH:mm")}</span>
          </div>
        </Col>

        <Col className="d-flex justify-content-end" md="4" sm="4" lg="4">
          <div className="d-flex  align-items-center">
            <span className="text-xs mr-2">{adversaryTeam.name}</span>
            <img
              className="player-img"
              src={`${BASE_URL}${adversaryTeam?.pictureUrl}`}
              alt="team"
            />
          </div>
        </Col>
      </div>
      <div className="d-flex flex-column mt-1">
        <Col
          onClick={() => openDetails(match)}
          className="bg-gray-200 d-flex justify-content-center cursor-pointer rounded"
        >
          <span className="text-sm">Editer</span>
        </Col>
        <Col
          onClick={() => handleDelete(id)}
          className="bg-red-400 d-flex justify-content-center cursor-pointer rounded mt-1"
        >
          <span className="text-sm text-white">Supprimer</span>
        </Col>
      </div>
    </div>
  );
};
const Status = ({ label, value, active, handleFilter }) => {
  return (
    <Col
      onClick={() => handleFilter(value)}
      className={classNames(
        "d-flex justify-content-center filter-item cursor-pointer",
        active === value && "active-filter"
      )}
    >
      <div className="">
        <span className="text-sm">{label}</span>
      </div>
    </Col>
  );
};
const PlayersContainer = (props) => {
  const [activeFilter, setActiveFilter] = React.useState("ALL");
  const { data, loading, refetch } = useMatchQuery({ status: activeFilter });
  const [selectedTeam, setSelectedTeam] = React.useState(null);
  const [state, setState] = React.useState({
    goalAdversaryTeam: 0,
    goalLocalTeam: 0,
  });

  React.useEffect(() => {}, [activeFilter]);
  const [visible, setVisible] = React.useState(false);
  const { deleteMatch, updateMatch } = useMatchMutation();
  const handleDelete = (matchId) => {
    props.showConfirmModal({
      title: "Supprimer ce match",
      confirmFunction: () => {
        deleteMatch({ matchId }).then((res) => {
          refetch();
        });
      },
    });
  };
  function openDetails(team = null) {
    setSelectedTeam(team);
    setState({
      goalAdversaryTeam: team?.goalAdversaryTeam,
      goalLocalTeam: team?.goalLocalTeam,
    });
    setVisible(!visible);
  }
  function handleFilter(filter) {
    setActiveFilter(filter);
  }
  function handleSubmit() {
    updateMatch({
      input: {
        id: selectedTeam?.id,
        goalAdversaryTeam: parseInt(state?.goalAdversaryTeam),
        goalLocalTeam: parseInt(state?.goalLocalTeam),
        winnerTeam:
          parseInt(state?.goalAdversaryTeam) === parseInt(state.goalLocalTeam)
            ? null
            : parseInt(state?.goalAdversaryTeam) >
              parseInt(state?.goalLocalTeam)
            ? selectedTeam?.adversaryTeam?.id
            : selectedTeam?.localTeam?.id,
      },
    }).then(() => {
      openDetails();
      setState({
        goalLocalTeam: 0,
        goalAdversaryTeam: 0,
      });
      refetch();
    });
  }
  function handleChangeInput(e) {
    const { name, value } = e.target;
    setState({ ...state, [name]: value });
  }
  const STATUS = [
    { label: "Tous", value: "ALL" },
    { label: "En attente", value: "WAITING" },
    { label: "Accepté", value: "ACCEPTED" },
    { label: "Réfusé", value: "REFUSED" },
    { label: "Finis", value: "FINISHED" },
  ];
  return (
    <div>
      <Col className="p-0">
        <Header
          bg={bg}
          title="List des matchs"
          titreClassNames="text-white"
          height="320px"
          containerStyle={{}}
        />
      </Col>
      <div className="main">
        <ModalDetails
          visible={visible}
          toggle={openDetails}
          title="Detail joueur"
          handleSubmit={handleSubmit}
        >
          <div className="d-flex justify-content-around">
            <div className="d-flex flex-column align-items-center">
              <img
                className="player-img"
                src={`${BASE_URL}${selectedTeam?.localTeam?.pictureUrl}`}
                alt="team"
              />
              <span className="text-sm">{selectedTeam?.localTeam?.name}</span>
              <Col md="4" sm="12" lg="4">
                <Input
                  className="bg-gray-100 w-20"
                  name="goalLocalTeam"
                  defaultValue={state?.goalLocalTeam}
                  onChange={handleChangeInput}
                />
              </Col>
            </div>
            <div>
              <span className="text-lg">vs</span>
            </div>
            <div className="d-flex flex-column align-items-center">
              <img
                className="player-img"
                src={`${BASE_URL}${selectedTeam?.adversaryTeam?.pictureUrl}`}
                alt="team"
              />
              <span className="text-sm">
                {selectedTeam?.adversaryTeam?.name}
              </span>
              <Col md="4" sm="12" lg="4">
                <Input
                  className="bg-gray-100 w-20"
                  name="goalAdversaryTeam"
                  defaultValue={state?.goalAdversaryTeam}
                />
              </Col>
            </div>
          </div>

          <div className="mt-4 d-flex   flex-column justify-content-center align-items-center"></div>
        </ModalDetails>
        <div className="mt-2">
          <div className="mt-2">
            <div
              className="d-flex flex-wrap bg-gray-200"
              style={{
                borderBottom: "0.5px solid #e8e9ef",
                borderTopRightRadius: 12,
                borderTopLeftRadius: 12,
              }}
            >
              {STATUS.map((st) => (
                <Status
                  {...st}
                  active={activeFilter}
                  handleFilter={handleFilter}
                />
              ))}
            </div>
          </div>
          <div className="mt-2">
            {data?.map((team) => (
              <PlayerItem
                {...team}
                openDetails={openDetails}
                handleDelete={handleDelete}
                match={team}
              />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};
export default withConfirmation(PlayersContainer);
