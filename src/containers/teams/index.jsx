import React from "react";
import { Card, Table, Row, Col, Button, CustomInput } from "reactstrap";
import { Trash, Eye } from "react-feather";
import withConfirmation from "../../hoc/withconfirmation";
import { useQueryTeams, useTeamMutation } from "../../hooks/teams.hooks";
import { BASE_URL } from "../../config/constants";
import ModalDetails from "./ModalDetails";

const PlayerItem = ({
	id,
	handleDelete,
	name,
	address,
	pictureUrl,
	createdBy,
	openDetails,
	team,
	playerCount,
}) => {
	return (
		<div className="d-flex player-item">
			<Col className="d-flex" md="10" sm="12" lg="10">
				<Col md="1" sm="2" lg="1">
					<img
						className="player-img"
						alt="player"
						src={`${BASE_URL}${pictureUrl}`}
					/>
				</Col>
				<Col className="d-flex flex-column" md="10" sm="12" lg="10">
					<span className="text-sm font-bold">{name}</span>
					<span className="text-xs">{address}</span>
					<span className="text-xs text-gray-600">
						Crée par:{" "}
						{`${createdBy?.player?.lastName} ${createdBy?.player?.firstName}`}
					</span>
					<div>
						<span className="text-xs">Nombre des joueurs : </span>
						<span className="badge badge-info">{playerCount}</span>
					</div>
				</Col>
			</Col>

			<Col md="2" sm="12" lg="2" className="d-flex align-items-center">
				<Button color="white" onClick={() => openDetails(team)}>
					<Eye color="blue" />
				</Button>
				<Button color="white" onClick={() => handleDelete(id)}>
					<Trash color="red" size={20} />
				</Button>
			</Col>
		</div>
	);
};

const TeamsContainer = (props) => {
	const { data, loading, refetch } = useQueryTeams();
	const [selectedTeam, setSelectedTeam] = React.useState(null);
	const [visible, setVisible] = React.useState(false);
	const { deleteTeam } = useTeamMutation();
	const handleDelete = (teamId) => {
		props.showConfirmModal({
			title: "Supprimer equipe",
			confirmFunction: () => {
				deleteTeam({ teamId }).then((res) => {
					console.log({ res });
					refetch();
				});
			},
		});
	};
	function openDetails(team = null) {
		setSelectedTeam(team);
		setVisible(!visible);
	}
	return (
		<div>
			
            <Col className="p-0">
                <Card className="p-2">
                <span className="font-semibold text-base text-primary-200 ">
					List des équipes
				</span>
                </Card>
				
			</Col>
			<ModalDetails visible={visible} toggle={openDetails}>
				<div>
					<img
						className="object-contain h-48 w-full"
						src={`${BASE_URL}${selectedTeam?.pictureUrl}`}
						alt="team"
					/>
				</div>
				<div className="mt-4 d-flex flex-column justify-content-center align-items-center">
					<Table striped>
						<tbody>
							<tr>
								<th>Nom equipe</th>
								<td>{selectedTeam?.name}</td>
							</tr>
							<tr>
								<th>Crée par:</th>
								<td>{`${selectedTeam?.createdBy?.player?.lastName} ${selectedTeam?.createdBy?.player?.firstName}`}</td>
							</tr>
							<tr>
								<th>Adresse</th>
								<td>{selectedTeam?.address}</td>
							</tr>
						</tbody>
					</Table>
				</div>
			</ModalDetails>
			<Card className="mt-2"> 
				<div className="mt-2">
					{data?.map((team) => (
						<PlayerItem
							{...team}
							openDetails={openDetails}
							handleDelete={handleDelete}
							team={team}
						/>
					))}
				</div>
			</Card>
		</div>
	);
};
export default withConfirmation(TeamsContainer);
