import React from 'react'
import { Modal, ModalBody, ModalHeader, ModalFooter, Button } from 'reactstrap'

export default function ModalDetails(props) {
    const {visible,children,...rest} =props;
    return (
        <Modal isOpen={visible} {...rest} >
            <ModalHeader className="bg-primary-200">
                <span className="text-white">Details joueur</span>
            </ModalHeader>
            <ModalBody>
              {children}
            </ModalBody>
            <ModalFooter>
                <Button className="bg-primary-200" onClick={rest.toggle}> 
                    Fermer
                </Button>
            </ModalFooter>
        </Modal>
    )
}
