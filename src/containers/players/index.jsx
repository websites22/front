import React from "react";
import { Card, Table,  Col, Button} from "reactstrap";
import { Trash, Eye } from "react-feather";
import withConfirmation from "../../hoc/withconfirmation";
import {useMutationPlayer, useQueryPlayers } from "../../hooks/teams.hooks";
import { BASE_URL } from "../../config/constants";
import ModalDetails from "./ModalDetails";

const PlayerItem = ({
	id,
	handleDelete,
    firstName,
    lastName,
    profile,
	pictureUrl,
	post,
	openDetails,
	player,
}) => {
	return (
		<div className="d-flex player-item">
			<Col className="d-flex" md="10" sm="12" lg="10">
				<Col md="1" sm="2" lg="1">
					<img
						className="player-img"
						alt="player"
						src={`${BASE_URL}${pictureUrl}`}
					/>
				</Col>
				<Col className="d-flex flex-column" md="10" sm="12" lg="10">
					<span className="text-sm font-bold">{lastName+ " "+firstName}</span>
					<span className="text-xs">{post}</span>
					
					<div>
						<span className="text-xs">Email: </span>
    <span className="text-xs">{profile?.email}</span>
					</div>
				</Col>
			</Col>

			<Col md="2" sm="12" lg="2" className="d-flex align-items-center">
				<Button color="white" onClick={() => openDetails(player)}>
					<Eye color="blue" />
				</Button>
				<Button color="white" onClick={() => handleDelete(id)}>
					<Trash color="red" size={20} />
				</Button>
			</Col>
		</div>
	);
};

const PlayersContainer = (props) => {
	const { data,  refetch } = useQueryPlayers();
    const [selectedTeam, setSelectedTeam] = React.useState(null);
    
	const [visible, setVisible] = React.useState(false);
	const { deletePlayer } = useMutationPlayer();
	const handleDelete = (playerId) => {
		props.showConfirmModal({
			title: "Supprimer equipe",
			confirmFunction: () => {
				deletePlayer({ playerId }).then((res) => {
					console.log({ res });
					refetch();
				});
			},
		});
	};
	function openDetails(team = null) {
		setSelectedTeam(team);
		setVisible(!visible);
	}
	return (
		<div>
			<Col className="p-0">
                <Card className="p-2">
                <span className="font-semibold text-base text-primary-200 ">
					List des joueurs
				</span>
                </Card>
				
			</Col>
			<ModalDetails visible={visible} toggle={openDetails} title="Detail joueur">
				<div>
					<img
						className="object-contain h-48 w-full"
						src={`${BASE_URL}${selectedTeam?.pictureUrl}`}
						alt="team"
					/>
				</div>
				<div className="mt-4 d-flex flex-column justify-content-center align-items-center">
					<Table striped>
						<tbody>
							<tr>
								<th>Nom joueur</th>
								<td>{selectedTeam?.firstName+ " "+ selectedTeam?.lastName}</td>
							</tr>
							<tr>
								<th>Email:</th>
								<td>{`${selectedTeam?.profile?.email}`}</td>
							</tr>
							<tr>
								<th>Poste</th>
								<td>{selectedTeam?.post}</td>
							</tr>
						</tbody>
					</Table>
				</div>
			</ModalDetails>
			<Card className="mt-2">
				<div className="mt-2">
					{data?.map((team) => (
						<PlayerItem
							{...team}
							openDetails={openDetails}
							handleDelete={handleDelete}
							player={team}
						/>
					))}
				</div>
			</Card>
		</div>
	);
};
export default withConfirmation(PlayersContainer);
