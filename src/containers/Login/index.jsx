import React, { useState } from "react";
import { Form, Input, FormGroup, Button } from "reactstrap";
import * as yup from "yup";
import Axios from "axios";
import { BASE_URL } from "../../config/constants";
import { useHistory } from "react-router-dom";
import Token from "../../helpers/Token";
export default function Login() {
    const history = useHistory();
    const [state,setState]=useState({
        email:"",
        password:""
    })
 React.useEffect(()=>{
     const tok=Token.getToken();
     if(!tok){
        window.location.reload();
        history.push("/home")
     }
     
           
     
 

        
      },[])
    const login=()=>{
 
        let schema = yup.object().shape({
            email:yup.string().required("Email required"),
            password:yup.string().required("Password required")
          });
          
          schema
            .validate(state)
            .then(function (valid) {
             Axios.post(BASE_URL+"/auth/login/admin",state).then(response=>{
                
                 const {user,admin,token} =response.data;
                 console.log({user,admin,token})
                //  if(!user || !token){
                //      return null;
                //  }
                // localStorage.setItem('token',token);
                 setTimeout(()=>{
                     window.location.reload();
                 },200)
                 
              
             })
            }).catch(er=>{
                console.log({er})
            });
          
    }

    const handleChnage=(e)=>{
        const {value,name} =e.target;
        setState({...state,[name]:value});
    }
	return (
		<div className="login d-flex justify-content-center align-items-center login-container"  >
			<div className="login-form d-flex flex-column justify-content-center align-items-center">
				<div>
					<span className="text-xl font-bold">Footify connexion</span>
				</div>
				<Form className="mt-2">
					<FormGroup>
						<Input type="text" placeholder="Email" name="email" value={state?.email} onChange={handleChnage} />
					</FormGroup>
					<FormGroup>
						<Input placeholder="password" type="password" name="password"  onChange={handleChnage}/>
					</FormGroup>
					<Button className="" onClick={login}>Se connecter</Button>
				</Form>
				<div className="mt-2">
					<a href="#"> Mot de passe oublié</a>
				</div>
			</div>
		</div>
	);
}
