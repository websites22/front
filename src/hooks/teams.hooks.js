const { useQuery, useMutation } = require("react-apollo")
const { GET_TEAMS, DELETE_EQUIPE, GET_PLAYERS, DELETE_PLAYER, FETCH_MATCHS, DELETE_MATCH, UPDATE_MATCH } = require("../graphql.tags")

export const useQueryTeams=()=>{
    const {data,loading,...rest}=useQuery(GET_TEAMS);
    return {
        data:data?.getTeams,
        loading,
        ...rest
    }
}
export const useTeamMutation =()=>{
    const [deleteTea] =useMutation(DELETE_EQUIPE);
    function deleteTeam({teamId}){
       return  deleteTea({
            variables:{
                teamId
            }
        })
    }
    return {deleteTeam}
    
}
export const useQueryPlayers=()=>{
    const {data,loading,...rest}=useQuery(GET_PLAYERS);
    return {
        data:data?.getPlayers,
        loading,
        ...rest
    }
}
export const useMutationPlayer =()=>{
    const [deletePlayers] =useMutation(DELETE_PLAYER);
    function deletePlayer({playerId}){
       return  deletePlayers({
            variables:{
                playerId
            }
        })
    }
    return {deletePlayer}
    
}
export const useMatchQuery =({status})=>{
    const {data,...rest}= useQuery(FETCH_MATCHS,{
        variables:{
            invitation:false,
            status
        }
    });
    return {
        data:data?.getMatchsAdversary,
        ...rest
    }
}
export const useMatchMutation=()=>{
    const [deleteM] =useMutation(DELETE_MATCH);
    const [updateM] =useMutation(UPDATE_MATCH)
    function deleteMatch({matchId}){
        return deleteM({
            variables:{
                matchId
            }
        })
    }
    function updateMatch({input}){
        return updateM({
            variables:{
                input
            }
        })
    }
    return {deleteMatch,updateMatch}
    
    
}