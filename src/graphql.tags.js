import gql from "graphql-tag";
export const GET_TEAMS=gql`
query{
    getTeams{
        id
        name
        address
        pictureUrl
        playerCount
        createdBy{
            player{
                id
                firstName
                lastName
            }
        }
    }
}
`;
export const DELETE_EQUIPE =gql`
mutation deleteTeam($teamId:ID){
    deleteTeam(teamId:$teamId)
}
`;

export const GET_PLAYERS =gql`
query {
    getPlayers {
			id
			firstName
			lastName
			pictureUrl
			post
			invitations {
				team {
					id
				}
			}
			profile {
				email
			}
		
	}
}
`
export const DELETE_PLAYER =gql`
mutation deletePlayer($playerId:ID){
    deletePlayer(playerId:$playerId)
}
`;

export const DELETE_MATCH =gql`
mutation deleteMatch($matchId:ID){
    deleteMatch(matchId:$matchId){
        id
    }
}
`;

export const UPDATE_MATCH = gql`
	mutation updateMatch($input: MatchUpdateInput) {
		updateMatch(input: $input) {
			id
		}
	}
`;

export const FETCH_MATCHS = gql`
	query getMatchsAdversary($status: String, $invitation: Boolean) {
		getMatchsAdversary(status: $status, invitation: $invitation) {
			id
			address
			stadium
			createdAt
			meetingTime
			dateMatch
			timeStartMatch
			meetingDate
			status
			timeEndMatch
            goalLocalTeam
            goalAdversaryTeam
			adversaryTeam {
				id
				name
				description
				pictureUrl
			}
			localTeam {
				id
				name
				pictureUrl
			}
			createdBy {
				player {
					firstName
					lastName
				}
			}
		}
	}
`;